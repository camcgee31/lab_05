#!/bin/bash
# For this assiment I combained the 2 scripts into one.
# It allows you to set view and delete elements 

# Set remberfile to the home directory and set it to a hiddent file nameed remember
rememberfile="$HOME/.remember"

# set input to be blank
input1=" "
# while input does not = exit keep running the script
while [ "$input1" != "exit" ]
do
# print the 4 options and read user input
	echo "1:)Set Reminder"
	echo "2:)Remind me"
	echo "3:)Delete Reminder"
	echo "Type exit to exit"
	read input1
#if user input = 1 set remind		
	if [ "$input1" = "1" ]
	then
		
		#set input2 to blank and if user types input 2 then exit if not record the entry and input it to remberfile			
		input2=" "
		while [ "$input2" != "exit" ]
		do
			echo $input2 >> $rememberfile
			echo "Enter reminder, end by typing exit "
			read input2
		done
	#if input1 = 2 then set arr then read remberfile and remove any blank space.
	# then read file line by line and input each line into array
	elif [ "$input1" = "2" ]
	then
		arr=()
		file=$(cat $rememberfile | tr -d "[:blank:]")
		for line in $file
		do
			arr+=("$line")
		done
		number=0
	# then read from array element by element add a number to the left and add it by one then echo the element and number
		for i in "${arr[@]}"
		do 
			echo "$number : $i"
			((number++))
		done
	# if input1 is 3 then set array and set file to rember file and remove blank space
	elif [ "$input1" = "3" ]
	then
		arr=()
		file=$(cat $rememberfile | tr -d "[:blank:]")
	# Read file line by line and input each line into arr
		for line in $file
		do
			arr+=("$line")
		done
	# then read from array element by element add a number to the left and add it by one then echo the element and number
		number=0
		for i in "${arr[@]}"
		do 
			echo "$number : $i"
			((number++))
		done
	# get user input about linenumber and remove that elment number from arr
		echo "Type the line number to delete reminder"
		read linenumber
		unset "arr[$(($linenumber))]"
	# clear remeberfile then read arr element by element and input each element in rember file
		>$rememberfile
		for i in "${arr[@]}"
		do 
			echo $i >> $rememberfile
		done
	fi
done
