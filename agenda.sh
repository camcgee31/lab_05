#!/bin/bash

# This is for the #26 Keeping Track of Events - Wicked Cool Shell Scripts p90 

#Set agendafile to a string that contains location of agenda
agendafile="$HOME/.agenda"

##########################
# ismonthname checks to make sure that the user inputed the full months name
# I tryed to do it the way the lab did it but I could not figure out how to compare user inputed month to the month in side the agenda file
isMonthName()
{
	echo $1
	case $(echo $1 | tr '[[:upper:]]' '[[:lower:]]') in
		january|february|march|april|may|june) return 0 ;;
		july|august|september|october|november|december) return 0 ;;
		* ) return 1 ;;
	esac
}

#########################
# This if statment just checks that you can write to the user home file. Some linux users dont have a home so this script will not work
if [ ! -w $HOME ]
then
	echo "$0: cannot write in your home directory ($HOME)" >&2
	exit 1
fi

#########################
# Ask user for 2 numbered options then read their input
echo "Agenda: Set a Reminder or check on the agenda"
echo "1:) Set reminder"
echo "2:) Check agenda"
echo "Type number for option"
read input1
echo ""
#any echo "" is their for a clear space.
# if user onput = 1 then they will be given the option to add agenda entires 
if [ "$input1" = "1" ]
then
	# Set one or first entry to 0
	month=" "
	#while the first entry or month does not = exit continue loop
	while [ "$month" != "exit" ]
	do
			# I had troubles haveing the user be able to input the date diffrently. If I had more time I could figure out
			# how to alow the user to do things like having the month be a number
			echo "input data as month day year ex. april 13 2022"
			read month day year
			
			######################
			# If the date is less then 1 or grater then 31 then exit becouse anything else is not a true day
			if [ "$day"  -lt 1 -o "$day" -gt 31 ]
			then 
				echo "Day number can only be in range 1-31" 
				exit 1
			fi
			#######################
			
			# if the month name is not full and not a real month exit 
			isMonthName $month
			if [ "$?" = "1" ]
			then
				echo "Please input full month name"
				exit 1
			fi
			# ask for the user discription
			echo "Reminder discription"
			read discription 
			
			# I found it eaiser to put the discruptio in "" 
			# Add the month day year and discription in a new line in agenda file
			echo $month $day $year \"$discription\" >>$agendafile
			
			
	done	
# if the user inputed 2 then they will be given the option to view their agenda
# in the future if I had more time I would add a method for users to view dates by month or day or year
elif [ "$input1" = "2" ]
then
	# while user did not input exit keep running the loop
	agendainput=""
	while [ "$agendainput" != "exit" ]
	do
		# display the options to the user then read user input
		echo ""
		echo "1:) view all"
		echo "2:) Check agenda by date"
		echo "Type number for option or type exit to exit"
		read agendainput
		echo ""
		
		# if the user inputed 1 then they will be given all the dates
		if [  "$agendainput" = "1" ]
		then
			arr=()
			# I added the underline in the lines becouse befor each word was put in their own array. This helped put each line in their own array
			# read file line by line then add _ in the blank spaces then print out each line and removethe _
			file=$(cat $agendafile | sed -e 's/ /_/g')
			for line in $file
			do
				echo $line | sed -e 's/_/ /g'
			done
		elif [ "$agendainput" = "2" ]
		then
			arr=()
			# I added the underline in the lines becouse befor each word was put in their own array. This helped put each line in their own array
			# each line is in their own element in arr
			file=$(cat $agendafile | sed -e 's/ /_/g')
			for line in $file
			do
				arr+=("$line")
			done
			input2=" "
			
			# print out the option to the user. They must have the date the same as the example. I tried otherwise but it was eiser to do it this way
			echo ""
			echo "Input month day yar ex. april 13 2022 to view that dates agenda"
			echo "Type exit to exit"
			read month day year
			echo "On $month $day $year you have "	
			# in this next part it takes each line removes the _ and puts each word in each element into their own element
			# I did this so I can read the month day and year eaiser 
			for i in "${arr[@]}"
			do 
				i=$(echo $i | sed -e 's/_/ /g')
				read -a array2 <<< "$i"
				if [ "${array2[0]}" = "$month" ] && [ "${array2[1]}" = "$day" ] && [ "${array2[2]}" = "$year" ] 
				then
					# Since repeating the date to the user is not practical I remove the date that is in the 3 first elements
					unset "array2[$((0))]"
					unset "array2[$((1))]"
					unset "array2[$((2))]"
					echo ${array2[@]} 
				fi 
				
			done
			echo ""
		fi
	done
fi
exit  0
